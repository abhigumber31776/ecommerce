package com.android.ecommerceApp.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    public DatabaseHelper(Context context) {
        super(context, "ProductList", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE cartInfo(itemId INTEGER,image TEXT,name TEXT,price FLOAT,quantity INTEGER)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public long insertData(String imageurl, String name, float price, int itemId) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("image", imageurl);
        values.put("name", name);
        values.put("price", price);
        values.put("itemId", itemId);
        values.put("quantity", 1);
        long l = db.insert("cartInfo", null, values);
        return l;
    }

    public int rowCount() {
        SQLiteDatabase db = getReadableDatabase();
        return db.rawQuery("SELECT name FROM cartInfo", null).getCount();
    }

    public Cursor viewData() {
        SQLiteDatabase db = getReadableDatabase();
        return db.rawQuery("SELECT * FROM cartInfo", null);
    }

    public void updateQuantity(int itemId) {
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * from cartInfo WHERE itemId = ?", new String[]{String.valueOf(itemId)});
        ContentValues values = new ContentValues();
        cursor.moveToFirst();
        values.put("quantity", cursor.getInt(4) + 1);
        cursor.close();
        db.update("cartInfo", values, "itemId = ?", new String[]{String.valueOf(itemId)});
    }

    public void removeItem(int itemId) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete("cartInfo", "itemId = ?", new String[]{String.valueOf(itemId)});
    }

    public String[] getCartProductsIds() {
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT itemId FROM cartInfo", null);
        String res[] = new String[cursor.getCount()];
        int i = 0;
        while (cursor.moveToNext()) {
            res[i] = String.valueOf(cursor.getInt(0));
            i++;
        }
        cursor.close();
        return res;
    }

    public String[] getCartProductsQuantity() {
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT quantity FROM cartInfo", null);
        String res[] = new String[cursor.getCount()];
        int i = 0;
        while (cursor.moveToNext()) {
            res[i] = String.valueOf(cursor.getInt(0));
            i++;
        }
        cursor.close();
        return res;
    }

    public void deleteAllItems() {
        SQLiteDatabase db = getWritableDatabase();
        db.delete("cartInfo", null, null);
    }
}
