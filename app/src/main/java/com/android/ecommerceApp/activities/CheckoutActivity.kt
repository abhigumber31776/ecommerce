package com.android.ecommerceApp.activities

import android.os.Bundle
import android.widget.CompoundButton
import android.widget.RadioGroup
import android.widget.Toast
import com.android.ecommerceApp.R
import kotlinx.android.synthetic.main.activity_checkout.*


class CheckoutActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_checkout)

        copyInfoCheckBox.setOnCheckedChangeListener(object :
            CompoundButton.OnCheckedChangeListener {

            override fun onCheckedChanged(p0: CompoundButton?, checked: Boolean) {
                if (checked) {
                    shipfirstName.setText(firstName.text.toString())
                    shiplastName.setText(lastName.text.toString())
                    shipaddress1.setText(address1.text.toString())
                    shipaddress2.setText(address2.text.toString())
                    shipcity.setText(city.text.toString())
                    shipstate.setText(state.text.toString())
                    shippostcode.setText(postcode.text.toString())
                    shipcountry.setText(country.text.toString())
                } else {
                    shipfirstName.setText("")
                    shiplastName.setText("")
                    shipaddress1.setText("")
                    shipaddress2.setText("")
                    shipcity.setText("")
                    shipstate.setText("")
                    shippostcode.setText("")
                    shipcountry.setText("")

                }
            }
        })

    }
}