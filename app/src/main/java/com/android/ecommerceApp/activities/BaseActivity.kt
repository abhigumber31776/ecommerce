package com.android.ecommerceApp.activities

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.android.ecommerceApp.R

import java.util.*

open class BaseActivity : AppCompatActivity() {
    private var alertDialog: AlertDialog? = null
    private var reqCode: Int = 0
    var progressBar: Dialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initProgress()

    }

    fun showProgress() {
        if (progressBar == null) {
            initProgress()
        }

        if (progressBar?.isShowing!!) {
            hideProgress()
        }

        try {
            if (!isFinishing) {
                progressBar!!.show()
            }
        } catch (e: Exception) {
            Log.e("", "" + e.localizedMessage)
        }


    }

    fun hideProgress() {
        if (progressBar == null) {
            initProgress()
        }
        progressBar!!.dismiss()
    }





    fun initProgress() {
        progressBar = Dialog(this)
        // window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        progressBar!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressBar!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressBar!!.setContentView(R.layout.loader_layout)
        progressBar!!.getWindow()!!.setBackgroundDrawableResource(android.R.color.transparent)
        progressBar!!.getWindow()!!.setGravity(Gravity.CENTER)
        progressBar!!.setCancelable(false)
    }



    fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            networkInfo?.isConnected ?: false
        } else false
    }

    fun iosAlert(message: String, context: Context) {
        val dialog = Dialog(context)
        dialog.setContentView(R.layout.ios_dialog);
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        dialog.setTitle("Title...");
        dialog.setCancelable(false)
        val ok = dialog.findViewById(R.id.ios_ok) as TextView
        val text = dialog.findViewById(R.id.ios_text) as TextView
        text.text = message

        ok.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }


    fun singleButtonDialog(msg: String?, button1Text: String = "Ok", button1Click: () -> Unit) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.ios_dialog)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val text =
            dialog.findViewById<View>(R.id.ios_text) as TextView
        text.text = msg

        val button1 =
            dialog.findViewById<View>(R.id.ios_ok) as TextView

        button1.text = button1Text
        button1.setOnClickListener {
            button1Click()
            dialog.dismiss()

        }
        dialog.show()
    }


    override fun onResume() {
        super.onResume()
    }


    fun changeStatusBarColor(activity:Activity){
        val window: Window = activity.getWindow()
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(activity, R.color.colorPrimary)
    }


    fun showToast(message: String) {
        Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
    }





}