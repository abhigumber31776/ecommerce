package com.android.ecommerceApp.models

import java.io.Serializable

data class ProductObj (var id:Int,var title:String,var price:Float,var description:String,var category:String,var image:String):Serializable