package com.android.ecommerceApp.models

import java.io.Serializable

data class CartPojo (var name:String,var image:String,var price:String,var qty:String,var id:Int):Serializable