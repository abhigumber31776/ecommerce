package com.android.ecommerceApp.activities

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.ecommerceApp.R
import com.android.ecommerceApp.adapters.ProductsAdapter
import com.android.ecommerceApp.apiServices.RetrofitClientInstance
import com.android.ecommerceApp.models.ProductObj
import kotlinx.android.synthetic.main.activity_description.*
import kotlinx.android.synthetic.main.activity_description.descToolbar
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : BaseActivity() {
    var adapter: ProductsAdapter? = null
    var layoutManager: RecyclerView.LayoutManager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        getData()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.action_cart) {
            startActivity(Intent(this, CartActivity::class.java))
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.menu_description, menu)
        return super.onCreateOptionsMenu(menu)
    }

    fun setAdapter(list: ArrayList<ProductObj>) {
        layoutManager = GridLayoutManager(this, 2)
        recycle_view.setLayoutManager(layoutManager)
        adapter = ProductsAdapter(list,this)
        recycle_view.setAdapter(adapter)

    }

    fun getData() {
        showProgress()
        RetrofitClientInstance.getAPI()
            ?.fetchAllProducts()
            ?.enqueue(object : Callback<ArrayList<ProductObj>> {
                override fun onFailure(call: Call<ArrayList<ProductObj>>?, t: Throwable?) {
                  hideProgress()
                }

                override fun onResponse(
                    call: Call<ArrayList<ProductObj>>?,
                    response: Response<ArrayList<ProductObj>>?
                ) {
                    hideProgress()
                    if (response!!.isSuccessful) {
                        val userpojo = response?.body()
                            if (userpojo != null) {
                            setAdapter(userpojo)

                            } else {
                               iosAlert(
                                    getString(R.string.pleasetryagain),
                                    this@MainActivity
                                )
                            }

                    } else {
                        iosAlert(
                            getString(R.string.pleasetryagain),
                          this@MainActivity
                        )
                    }
                }

            })
    }




}