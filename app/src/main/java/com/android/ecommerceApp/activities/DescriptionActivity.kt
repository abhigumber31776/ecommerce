package com.android.ecommerceApp.activities

import android.content.Intent
import android.database.Cursor
import android.graphics.Paint
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.android.ecommerceApp.R
import com.android.ecommerceApp.apiServices.RetrofitClientInstance
import com.android.ecommerceApp.helper.DatabaseHelper
import com.android.ecommerceApp.models.ProductObj
import com.example.spotme.spotMeUtills.loadUrl
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_cart.*
import kotlinx.android.synthetic.main.activity_description.*
import kotlinx.android.synthetic.main.toolbar.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class DescriptionActivity : BaseActivity() {
    var id = -1
    lateinit var product: ProductObj
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_description)
        setSupportActionBar(descToolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        descToolbar.setNavigationOnClickListener(View.OnClickListener { onBackPressed() })

        id = intent.getIntExtra("id", -1)
        getData(id)
        setListeners()
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.action_cart) {
            startActivity(Intent(this, CartActivity::class.java))
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.menu_description, menu)
        return super.onCreateOptionsMenu(menu)
    }


    fun getData(id: Int) {
        showProgress()
        RetrofitClientInstance.getAPI()
            ?.fetchSingleProduct(id)
            ?.enqueue(object : Callback<ProductObj> {
                override fun onFailure(call: Call<ProductObj>?, t: Throwable?) {
                    hideProgress()
                }

                override fun onResponse(
                    call: Call<ProductObj>?,
                    response: Response<ProductObj>?
                ) {
                    hideProgress()
                    if (response!!.isSuccessful) {
                        val userpojo = response?.body()
                        if (userpojo != null) {
                            setData(userpojo)
                            product = userpojo

                        } else {
                            iosAlert(
                                getString(R.string.pleasetryagain),
                                this@DescriptionActivity
                            )
                        }

                    } else {
                        iosAlert(
                            getString(R.string.pleasetryagain),
                            this@DescriptionActivity
                        )
                    }
                }

            })
    }


    fun setData(data: ProductObj) {
        if (!data.image.isNullOrEmpty()) {
            product_img.loadUrl(data.image)

        }
        desItemName.text = data.title
        desItemPrice.text = "Price: $" + data.price
        regularPrice.text = "$" + data.price
        regularPrice.setPaintFlags(regularPrice.getPaintFlags() or Paint.STRIKE_THRU_TEXT_FLAG)
        itemDescription.text = data.description

    }


    fun addToCart() {
        val helper = DatabaseHelper(applicationContext)
        val cursor: Cursor = helper.viewData()
        while (cursor.moveToNext()) {
            if (id == cursor.getInt(0)) {
                Snackbar.make(
                    findViewById(android.R.id.content),
                    "Item Inserted",
                    Snackbar.LENGTH_SHORT
                ).show()
                helper.updateQuantity(id)
                return
            }
        }
        val l: Long = helper.insertData(product.image, product.title, product.price, id)
        if (l == -1L) {
            Snackbar.make(
                findViewById(android.R.id.content),
                "Item Not Inserted",
                Snackbar.LENGTH_SHORT
            ).show() //Toast.makeText(this, "Item Not Inserted", Toast.LENGTH_SHORT).show();
        } else Snackbar.make(
            findViewById(android.R.id.content),
            "Item Inserted",
            Snackbar.LENGTH_SHORT
        ).show()
    }

    fun setListeners() {
        addtocart.setOnClickListener() {
            addToCart()
        }

        buy.setOnClickListener(){
            val intent=Intent(this,CheckoutActivity::class.java)
            startActivity(intent)
        }

    }

}