package com.android.ecommerceApp.adapters

import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.android.ecommerceApp.R
import com.android.ecommerceApp.activities.DescriptionActivity
import com.android.ecommerceApp.models.ProductObj
import com.example.spotme.spotMeUtills.loadUrl
import java.util.*

class ProductsAdapter(
    var results: ArrayList<ProductObj>,
    private val context: Context

) : RecyclerView.Adapter<ProductsAdapter.ViewHolder>() {


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.product_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int
    ) {

        holder.itemView.animate().setInterpolator(AccelerateDecelerateInterpolator())
            .setDuration(300).scaleX(1f).scaleY(1f).start()

        if (!results[position].image.isNullOrEmpty()) {
            holder.product_img.loadUrl(results[position].image)
        }

        holder.product_desc.text = results[position].title
        holder.product_price.text = results[position].price.toString() + "$"
        holder.product_sale_price.text = results[position].price.toString() + "$"

        holder.itemView.setOnClickListener {
            val intent = Intent(context, DescriptionActivity::class.java)
            intent.putExtra("id",results[position].id)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }


    }

    override fun getItemCount(): Int {
        return if (results == null) 0 else results.size
    }


    inner class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var product_img: ImageView
        var product_desc: TextView
        var product_price: TextView
        var product_sale_price: TextView


        init {
            product_img = itemView.findViewById(R.id.recycle_product_image)
            product_desc = itemView.findViewById(R.id.recycle_product_title)
            product_price = itemView.findViewById(R.id.recycle_product_price)
            product_sale_price = itemView.findViewById(R.id.recycle_sale_price)
            product_sale_price.setPaintFlags(product_sale_price.getPaintFlags() or Paint.STRIKE_THRU_TEXT_FLAG)


        }
    }


}