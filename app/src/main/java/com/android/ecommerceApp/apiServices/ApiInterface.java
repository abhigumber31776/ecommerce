package com.android.ecommerceApp.apiServices;

import com.android.ecommerceApp.models.ProductObj;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("products/")
    Call<ArrayList<ProductObj>> fetchAllProducts();

    @GET("products/{id}")
    Call<ProductObj> fetchSingleProduct(@Path("id") Integer id);

}
