package com.android.ecommerceApp.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.android.ecommerceApp.R
import com.android.ecommerceApp.adapters.CartAdapter
import com.android.ecommerceApp.helper.DatabaseHelper
import com.android.ecommerceApp.models.CartPojo
import kotlinx.android.synthetic.main.activity_cart.*
import kotlinx.android.synthetic.main.activity_description.*
import java.util.*
import kotlin.collections.ArrayList

class CartActivity : BaseActivity() {

    var cartList = ArrayList<CartPojo>()
    var total = 0f
    var helper: DatabaseHelper? = null
    fun refresh() {
        helper = DatabaseHelper(applicationContext)
        val cursor = helper!!.viewData()
        cartList= ArrayList()
        cartList.clear()

        total = 0f
        while (cursor.moveToNext()) {
            total = total + cursor.getString(3).toFloat() * cursor.getString(4).toFloat()
            val cartPojo=CartPojo(cursor.getString(2),cursor.getString(1),cursor.getString(3),cursor.getString(4),cursor.getInt(0))
            cartList.add(cartPojo)

        }
        tv_total!!.text = "$$total"
        setImage()
        val adapter: RecyclerView.Adapter<*> = CartAdapter(
            this@CartActivity,
            applicationContext,
            cartList
        )
        cartRecyclerView.adapter = adapter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart)
        setSupportActionBar(cartToolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        cartToolbar.setNavigationOnClickListener(View.OnClickListener { onBackPressed() })
        val swipeRefreshLayout = findViewById<SwipeRefreshLayout>(R.id.swipe)
        helper = DatabaseHelper(applicationContext)
        val cursor = helper!!.viewData()
        swipeRefreshLayout.setOnRefreshListener {
            val cursor = helper!!.viewData()
            cartList.clear()

            total = 0f
            while (cursor.moveToNext()) {
                total = total + cursor.getString(3).toFloat() * cursor.getString(4).toFloat()
                val cartPojo=CartPojo(cursor.getString(2),cursor.getString(1),cursor.getString(3),cursor.getString(4),cursor.getInt(0))
                cartList.add(cartPojo)

            }
            tv_total.setText("$$total")
            val adapter: RecyclerView.Adapter<*> = CartAdapter(
                this@CartActivity,
                applicationContext,
                cartList
            )
            cartRecyclerView.adapter = adapter
            setImage()
            swipeRefreshLayout.isRefreshing = false
        }
        val manager: RecyclerView.LayoutManager = LinearLayoutManager(applicationContext)
        cartRecyclerView.setLayoutManager(manager)
        while (cursor.moveToNext()) {
            total = total + cursor.getString(3).toFloat() * cursor.getString(4).toInt()
            val cartPojo=CartPojo(cursor.getString(2),cursor.getString(1),cursor.getString(3),cursor.getString(4),cursor.getInt(0))
            cartList.add(cartPojo)

        }
        tv_total.setText("$$total")
        val adapter: RecyclerView.Adapter<*> = CartAdapter(
            this@CartActivity,
            applicationContext,
          cartList
        )
        cartRecyclerView.setAdapter(adapter)
        setImage()
        openCheckoutActivity()
        showText()
        setListeners()

    }

    private fun setImage() {
        val empty = findViewById<ImageView>(R.id.emptyCartImage)
        val db = DatabaseHelper(this)
        if (db.rowCount() == 0) {
            empty.visibility = View.VISIBLE
        } else empty.visibility = View.INVISIBLE
    }

    fun openCheckoutActivity() {
        val db = DatabaseHelper(this)
        if (db.rowCount() == 0) {
            buttonCheckkout.visibility = View.INVISIBLE
            linearLayout.visibility = View.INVISIBLE
        } else {
            buttonCheckkout.visibility = View.VISIBLE
        }
    }

    fun showText() {
        val db = DatabaseHelper(this)
        if (db.rowCount() == 0) {
            tv_total!!.visibility = View.INVISIBLE
            linearLayout.visibility = View.INVISIBLE
        } else {
            tv_total!!.visibility = View.VISIBLE
        }
    }

    fun setListeners() {
        buttonCheckkout.setOnClickListener(){
            val intent = Intent(this, CheckoutActivity::class.java)
            startActivity(intent)
        }

    }
}