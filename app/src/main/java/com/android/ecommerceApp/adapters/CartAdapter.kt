package com.android.ecommerceApp.adapters

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.android.ecommerceApp.R
import com.android.ecommerceApp.activities.CartActivity
import com.android.ecommerceApp.adapters.CartAdapter.CartViewHolder
import com.android.ecommerceApp.helper.DatabaseHelper
import com.android.ecommerceApp.models.CartPojo
import com.bumptech.glide.Glide
import java.util.*

 class CartAdapter(
    activity: Activity,
    private val context: Context,
    private val cartlist: ArrayList<CartPojo>
) : RecyclerView.Adapter<CartViewHolder>() {
    var cartActivity: CartActivity
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartViewHolder {
        val layout = LayoutInflater.from(parent.context)
            .inflate(R.layout.cart_item_layout, parent, false) as RelativeLayout
        return CartViewHolder(layout)
    }

    override fun onBindViewHolder(holder: CartViewHolder, position: Int) {
        holder.itemView.animate().setInterpolator(AccelerateDecelerateInterpolator())
            .setDuration(300).scaleX(1f).scaleY(1f).start()
        holder.name.text = cartlist[position].name
        holder.price.text = "$" + cartlist[position].price
        holder.quantity.text = "Qty: " + cartlist[position].qty
        Glide.with(context).load(cartlist[position].image).into(holder.image)
        holder.deleteItem.setOnClickListener {
            val helper = DatabaseHelper(context)
            helper.removeItem(cartlist[position].id)
            cartlist.removeAt(position)
            notifyDataSetChanged()
            cartActivity.refresh()
        }
    }

    override fun getItemCount(): Int {
        return cartlist.size
    }

     inner class CartViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name: TextView
        var price: TextView
        var quantity: TextView
        var image: ImageView
        var deleteItem: ImageButton

        init {
            deleteItem = itemView.findViewById(R.id.deleteItem)
            name = itemView.findViewById(R.id.cartItemName)
            price = itemView.findViewById(R.id.cartItemPrice)
            image = itemView.findViewById(R.id.cartItemImage)
            quantity = itemView.findViewById(R.id.cartItemQuantity)
        }
    }

    init {
        cartActivity = activity as CartActivity
    }
}